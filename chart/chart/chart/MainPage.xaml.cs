﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Microcharts;
using SkiaSharp;
using Entry = Microcharts.Entry;

namespace chart
{
    public partial class MainPage : ContentPage
    {
        public BarChart barChart;
        public DonutChart donutChart;
        public RadarChart radarChart;


        public MainPage()
        {
            InitializeComponent();
            CreateCharts(); 
            PopulatePicker();
        }

        private void PopulatePicker()
        {
            var Graph_Names = new List<string>()
            {
                "barchart","donutchart","radarchart"
              /*  new Graph()
                {
                    Graph_Name= "Bar Chart"
                },
                new Graph()
                {
                    Graph_Name= "Donut Chart"
                },
                new Graph()
                {
                    Graph_Name= "Radar Chart"
                },*/
            };
            MyPicker.ItemsSource = Graph_Names;

        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Picker myPicker = (Picker)sender;
            var Holder = myPicker.SelectedItem.ToString();
           // var Holder = myPicker.Title;
            if(Holder== "barchart")
            {
                Chart1.Chart = barChart;
            }
            else if(Holder== "donutchart")
            {
                Chart1.Chart = donutChart;
            }
            else if (Holder== "radarchart")
            {
                Chart1.Chart = radarChart;
            }
        }

        private void CreateCharts()
            {
                var entryList = new List<Entry>()
            {
                 new Entry(43)
                {
                    Label = "Apples",
                    Color = SKColor.Parse("#FF0000"),
                },
                new Entry(38)
                {
                    Label = "Oranges",
                    Color = SKColor.Parse("#FF6700"),
                },
                new Entry(55)
                {
                    Label = "Bananas",
                    Color = SKColor.Parse("#FFFF00"),
                }
            };
                 barChart = new BarChart() { Entries = entryList };
                 donutChart = new DonutChart() { Entries = entryList };
                 radarChart = new RadarChart() { Entries = entryList };
           
            Chart1.Chart = barChart;

        }



        }

    public class Graph
    {
        public string Graph_Name
        {
            get;
            set;
        }
        
        
    }
}
